<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 29.01.2019
 * Time: 08:15
 */

namespace App\Helper;

use App\Models\Booking;
use App\Models\User;
use App\Notifications\BookingCreate;
use App\Notifications\UserCreate;

class TeamBookingAPI
{

    protected $access_token;
    protected $auth_token;

    public function __construct()
    {
        $this->access_token = env('API_ACCESS_TOKEN');
        $this->auth_token = env('API_TOKEN');
    }

    /**
     * @param $url
     * @param $assoc
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function sendGet($url, $assoc = true)
    {
        $client  = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url,
            [
                'headers' => [
                    'Authorization' => sprintf('Basic %s', $this->access_token)
                ],
            ]
        );
        return json_decode($response->getBody()->getContents(), $assoc);
    }

    // RESERVATIONS LIST
    public function getReservationsList()
    {
        $url = env('API_URL').'&operation=get_reservations&auth_token='.$this->auth_token;
        return $this->sendGet($url);
    }

    // RESERVATION
    public function getReservationById($id)
    {
        $url = env('API_URL').'&operation=get_reservation&id='.$id.'&auth_token='.$this->auth_token;
        return $this->sendGet($url);
    }


    public static function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }


    private function checkUser($email)
    {
        return User::where('email',$email)->first();
    }

    private function extractEmail($array,$value)
    {
        foreach ($array as $key => $val) {
            if ($val['name'] == $value){
                return $val['value'];
            }
        }
        return null;
    }

    private function extractName($array,$first,$second)
    {
        $name = '';
        foreach ($array as $key => $val) {
            if (($val['name'] == $first)||($val['name'] == $second)) {
               $name .= $val['value'].' ';
            }
        }
        return $name;
    }

    private function createUser($name,$email,$password)
    {
        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->save();
        $user->assignRole('User');

        return $user;
    }

    private function createBooking($user,$result)
    {
        $booking = Booking::create([
            'user_id' => $user->id,
            'reservation_id' => $result['id'],
            'creationDateTime' => date("Y-m-d H:i:s",$result['creationDateTime']),
            'booking_date' => date("Y-m-d H:i:s",$result['datetime']['start']),
            'start' => date("Y-m-d H:i:s",$result['datetime']['start']),
            'end' => date("Y-m-d H:i:s",$result['datetime']['end']),
            'userTimezone' => $result['customerTimezone'] ,
            'status' => 0,
            'hash' => uniqid(),
        ]);

        // Change user password for new booking
//        if(Booking::where('user_id',$user->id)->count() >= 1) {
//            $password = self::randomPassword();
//            $user->password = $password;
//            $user->save();
//        } else {
//            $password = '';
//        }

//        $user->notify(new BookingCreate($user,$password,$booking));
        $user->notify(new BookingCreate($user,$booking));
    }

    public function parseResult($results)
    {
        if(! empty($results['response'])) {
            foreach ($results['response'] as $result) {
                if ($result['status'] == 'confirmed') {
                    $booking = Booking::where('reservation_id', $result['id'])->first();
                    if (is_null($booking)) {
                        $email = $this->extractEmail($result['formFields'], 'email');
                        $user = $this->checkUser($email);
                        if(is_null($user)) {
                            $userName = trim($this->extractName($result['formFields'],'first_name','second_name'));
                            // New user
                            $password = self::randomPassword();
                            $user = $this->createUser($userName,$email,$password);
                        }

                        $this->createBooking($user,$result);
                    }
                }
            }
        }
    }
}
