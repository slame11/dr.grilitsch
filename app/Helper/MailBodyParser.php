<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 22.11.2018
 * Time: 12:16
 */

namespace App\Helper;

use DateTime;
use App\Events\RegisterBooking;

class MailBodyParser
{
    protected $rules = [
        '/(?<=Datum)(.*)(?=Beginn)/',
        '/(?<=Beginn)(.*)(?=Ende)/',
        '/(?<=Ende)(.*)(?=Erstellt)/',
        '/(?<=EMail)(.*)(?=Name)/',
        '/(?<=Name)(.*)(?=Phone)/',
        '/(?<=Phone)(.*)(?=Description)/',
        ];

    public static function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

    public function parsEmail($text)
    {
        $results = [];
        foreach($this->rules as $rule) {
            preg_match($rule, $text, $matches);
            if(! empty($matches)) {
                $str = trim(preg_replace('/\s+/', ' ', $matches[0]));
                array_push($results, $str);
            }
        }
        // Create new Booking
        event(new RegisterBooking($results));
    }

    public function parsDate($text)
    {
        $rules = ['/[a-zA-z]+/','/(\d+)(?=,)/','/(?<=\, )(\d+)/'];
        $results = [];
        //Dezember 20, 2018
        //return DateTime::createFromFormat('F d, Y', $text)->format('Y-m-d');
        foreach($rules as $rule) {
            preg_match($rule, $text, $matches);
            if (! empty($matches)) {
                array_push($results, $matches[0]);
            }
        }
        switch ($results[0]){
            case "Januar":
                $month = '01';
                break;
            case "Februar":
                $month = '02';
                break;
            case "März":
                $month = '03';
                break;
            case "April":
                $month = '04';
                break;
            case "Mai":
                $month = '05';
                break;
            case "Juni":
                $month = '06';
                break;
            case "Juli":
                $month = '07';
                break;
            case "August":
                $month = '08';
                break;
            case "September":
                $month = '09';
                break;
            case "Oktober":
                $month = '10';
                break;
            case "November":
                $month = '11';
                break;
            case "Dezember":
                $month = '12';
                break;
        }

        return $results[2]."-".$month."-".$results[1];
    }
}
