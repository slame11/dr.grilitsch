<?php

namespace App\Listeners;

use App\Events\RegisterBooking;
use App\Helper\MailBodyParser;
use App\Models\Booking;
use App\Models\User;
use App\Notifications\BookingCreate;
use App\Notifications\UserCreate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateBooking
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    private function createUser($data,$password)
    {
        $user = new User();
        $user->name = $data[4];
        $user->email = $data[3];
        $user->password = $password;
        $user->save();
        $user->assignRole('User');

        return $user;
    }

    private function createBooking($data,$user,$startdate)
    {
        // New booking
        $booking = new Booking();
        $booking->user_id = $user->id;
        $booking->booking_date = $startdate;
        $booking->start = $data[1];
        $booking->end = $data[2];
        $booking->status = 0;
        $booking->hash = uniqid();
        $booking->save();

        $user->notify(new UserCreate($user,$password));
        $user->notify(new BookingCreate($user,$booking));
    }

    private function checkBooking($data,$user,$startdate)
    {
        $booking = Booking::where([
            ['user_id',$user->id],
            ['booking_date',$startdate],
            ['start',$data[1]],
            ['end',$data[2]]
        ])->first();

        return $booking;
    }


    /**
     * Handle the event.
     *
     * @param  RegisterBooking  $event
     * @return void
     */
    public function handle(RegisterBooking $event)
    {
        $data = $event->data;
        // Convert date
        $helper = new MailBodyParser();
        $startdate = $helper->parsDate($data[0]);

        // Check user
        $user = User::where('email',$data[3])->first();
        if($user) {
            $booking = $this->checkBooking($data,$user,$startdate);
            // New booking
            if(! $booking) {
                $this->createBooking($data, $user, $startdate);
            }
        }else{
            // New user
            $password = MailBodyParser::randomPassword();
            $user = $this->createUser($data,$password);

            // New booking
            $this->createBooking($data,$user,$startdate);
        }
    }
}
