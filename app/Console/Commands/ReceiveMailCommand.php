<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helper\MailBodyParser;
use Webklex\IMAP\Facades\Client;

class ReceiveMailCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'receive:emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Receive Mails from CMS';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $oClient = Client::account('default');
        $oClient->connect();
        $oFolder = $oClient->getFolder('INBOX');
        $oMessages = $oFolder->query()->from(env('IMAP_FILTER'))->get();
        foreach ($oMessages as $oMessage) {
            if($oMessage->hasHTMLBody()) {
                $html = $oMessage->getHTMLBody();
                $document = new \DOMDocument('1.0', 'UTF-8');
                // set error level
                $internalErrors = libxml_use_internal_errors(true);
                // load HTML
                $document->loadHTML($html);
                // Restore error level
                libxml_use_internal_errors($internalErrors);
                $tr = $document->getElementsByTagName('tr');
                $text = '';
                foreach ($tr as $item) {
                    $text .= $item->nodeValue;
                }
                if($text != "") {
                    $helper = new MailBodyParser();
                    $text = trim(preg_replace('/\s+/', ' ', $text));
                    $helper->parsEmail($text);
                }
            }
            $oMessage->delete();
        }
    }
}
