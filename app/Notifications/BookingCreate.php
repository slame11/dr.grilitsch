<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class BookingCreate extends Notification
{
    use Queueable;

    private $user;
    private $password;
    private $booking;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
//    public function __construct($user,$password,$booking)
    public function __construct($user,$booking)
    {
        $this->user = $user;
//        $this->password = $password;
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->line('Dear, '.$this->user->name.' you created a booking')
            ->line('please use your password, '.$this->booking->hash.' for login.')
            ->line('Booking on '.$this->booking->booking_date.' at '.$this->booking->start)
            ->action('Please use link to start chat', url('/verification?email='.$this->user->email))
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
