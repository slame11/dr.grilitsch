<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 21.11.2018
 * Time: 16:49
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class HasPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @param string $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if(null == Auth::user()){
            return redirect()->route('login');
        }
        if (!Auth::user()->hasPermissionTo($permission)) {
            abort('401');
        }

        return $next($request);
    }
}
