<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Admin\AdminController;
use App\Models\ChatFile;
use Illuminate\Http\Request;

class ChatFileController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $files = $request->file('file');
        foreach($files as $file) {
            ChatFile::create([
                'room_id' => $request->input('room_id'),
                'title' => $file->getClientOriginalName(),
                'description' => 'Upload file from chat room:'.$request->input('room_id'),
                'path' => $file->store('public/files/'.$request->input('room_id')),
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function show(ChatFile $chatFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function edit(ChatFile $chatFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ChatFile $chatFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ChatFile  $chatFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ChatFile $chatFile)
    {
        //
    }
}
