<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 10.01.2019
 * Time: 11:39
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $users = User::whereNotIn('id',User::role('Admin')->pluck('id'))->orderBy('name')->paginate(10);
        return view('admin.user.index',['users'=>$users]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {

        $user->delete();
        return redirect()->route('users.index');
    }
}
