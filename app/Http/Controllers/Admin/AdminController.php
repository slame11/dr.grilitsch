<?php

namespace App\Http\Controllers\Admin;

use App\Helper\MailBodyParser;
use App\Helper\TeamBookingAPI;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Webklex\IMAP\Facades\Client;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.layouts.admin-lte');
    }

    public function getMails()
    {
        $oClient = Client::account('default');
        $oClient->connect();
        $oFolder = $oClient->getFolder('INBOX');
        $oMessages = $oFolder->query()->from(env('IMAP_FILTER'))->get();
        foreach ($oMessages as $oMessage) {
            if($oMessage->hasHTMLBody()) {
                $html = $oMessage->getHTMLBody();
                $document = new \DOMDocument('1.0', 'UTF-8');
                // set error level
                $internalErrors = libxml_use_internal_errors(true);
                // load HTML
                $document->loadHTML($html);
                // Restore error level
                libxml_use_internal_errors($internalErrors);
                $tr = $document->getElementsByTagName('tr');
                $text = '';
                foreach ($tr as $item) {
                    $text .= $item->nodeValue;
                }
                if($text != "") {
                    $helper = new MailBodyParser();
                    $text = trim(preg_replace('/\s+/', ' ', $text));
                    $helper->parsEmail($text);
                }
            }
            $oMessage->delete();
        }

        return redirect()->route('admin-lte');
    }

    public function getReservations()
    {
        $tbAPI = new TeamBookingAPI();
        $results = $tbAPI->getReservationsList();
        $tbAPI->parseResult($results);

        return redirect()->route('admin-lte');
    }
}
