<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 28.11.2018
 * Time: 14:41
 */

namespace App\Http\Controllers;

use App\Events\MessageSent;
use App\Models\ChatFile;
use Auth;
use App\Models\Booking;
use App\Models\Room;
use App\Models\User;
use Illuminate\Http\Request;
use DateTime;

use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class ChatController extends Controller
{

    protected $sid;
    protected $token;
    protected $key;
    protected $secret;

    public function __construct()
    {
        $this->sid = config('services.twilio.sid');
        $this->token = config('services.twilio.token');
        $this->key = config('services.twilio.key');
        $this->secret = config('services.twilio.secret');
    }

    public function checkExistRoom($booking)
    {
        return Room::where('booking_id',$booking->id)->first();
    }

    public function createChat($booking)
    {
        $room = Room::create(['booking_id'=>$booking->id]);

        $users = [];
        array_push($users, $booking->user_id);
        $admins = User::role('Admin')->get();
        if($admins->count() > 1) {
            foreach($admins as $admin) {
                array_push($users, $admin->id);
            }
        }else{
            array_push($users, $admins->first()->id);
        }
        $room->users()->attach(array_unique($users));

        return $room;
    }

    public function checkChat(Request $request)
    {
        if($request->has('email') && $request->has('hash')) {
        	$user = User::where('email', $request->input('email'))->first();
            $booking = Booking::where('hash',$request->input('hash'))->first();

            // Change time zone in app.conf on production server
            $now = \Carbon\Carbon::now();

            if($booking) {
                if($user){
			        Auth::loginUsingId($user->id);
		        }

                $booking_date = new DateTime($booking->booking_date.' '.$booking->start);
                if($booking_date > $now) {
                    $interval = $now->diff($booking_date);
                    if($interval->y == 0 && $interval->m == 0 && $interval->d == 0 && $interval->h == 0 && $interval->i <= 5) {
                        // Create chat
                        $room = $this->checkExistRoom($booking);
                        if(! $room){
                            $room = $this->createChat($booking);
                            return redirect()->route('chat',['room'=>$room->id]);
                        }else{
                            return redirect()->route('chat',['room'=>$room->id]);
                        }
                    }
                }else{
                    $interval = $now->diff($booking_date);
                    if($interval->y == 0 && $interval->m == 0 && $interval->d == 0 && $interval->h == 0 && $interval->i <= 35) {
                        // Create chat
                        $room = $this->checkExistRoom($booking);
                        if(! $room){
                            $room = $this->createChat($booking);
                            return redirect()->route('chat',['room'=>$room->id]);
                        }else{
                            return redirect()->route('chat',['room'=>$room->id]);
                        }
                    }
                }

                return view('chat.notready',['booking'=>$booking]);
            }else{
                return view('chat.notfound');
            }
        }
    }

    public function verification(Request $request)
    {
//        if($request->has('email') && $request->has('hash')) {
        if($request->has('email')) {
            return view('chat.login',
                [
                    'email' => request('email'),
//                    'hash' => request('hash'),
                ]
            );
        }
    }

    public function srvtime(Request $request)
    {
        return \Carbon\Carbon::now()->toDateTimeString();
    }

    public function checkRoom(Request $request)
    {
        if($request->has('booking')) {
            $booking = Booking::find($request->input('booking'));
            $room = $this->checkExistRoom($booking);
            if(isset($room->id)){
                return redirect()->route('chat',['room'=>$room->id]);
            }else{
                $room = $this->createChat($booking);
                return redirect()->route('chat',['room'=>$room->id]);
            }
        }
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchMessages(Room $room)
    {
        return $room->messages()->with('user')->get();
    }

    /**
     * Persist message to database
     *
     * @param  Request $request
     * @return Response
     */
    public function sendMessage(Request $request)
    {
        $user = Auth::user();

        if($request->has('room_id')) {
            $roomId = $request->room_id;

            $message = $user->messages()->create([
                'room_id' => $roomId,
                'message' => $request->input('message'),
            ]);
        }

        broadcast(new MessageSent($user, $message, $roomId));

        return ['status' => 'Message Sent!', 'created_at' => $message->created_at];
    }

    /**
     * @param Request $request
     */
    public function dropzone(Request $request)
    {
        $file = $request->file('file');
        ChatFile::create([
            'room_id' => $request->input('room_id'),
            'title' => $file->getClientOriginalName(),
            'description' => 'Upload file from chat room:'.$request->input('room_id'),
            'path' => $file->store('public/files/'.$request->input('room_id')),
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getFiles(Request $request)
    {
        if($request->has('room-id')){
            $room = Room::find($request->input('room-id'));
            return view('chat.partials.result', ['room' => $room]);
        }
    }

    public function deleteFile(Request $request)
    {
        if($request->has('file')) {
            $file = ChatFile::find($request->input('file-id'));
            $file->delete();
            Storage::delete($request->input('file'));

            $room = Room::find($request->input('room-id'));
            return view('chat.partials.result', ['room' => $room]);
        }
    }


    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Room $room, User $user = null)
    {
        $this->middleware('auth');
        $user = Auth::user();

//        if(! $user) {
//            return redirect()->route('login');
//        }

        $booking = Booking::find($room->booking_id);
        $now = \Carbon\Carbon::now();

        if(method_exists($user, 'hasPermissionTo') && ! $user->hasPermissionTo('admin')) {
            if ($booking) {

                $booking_start = \Carbon\Carbon::parse($booking->booking_date.' '.$booking->start)->addMinute(-5);
                //$booking_end = \Carbon\Carbon::parse($booking_start)->addMinute(35);

                if ($booking_start > $now) {
                    $interval = $now->diff($booking_start);
                       if($interval->invert = 0 && $interval->y == 0 && $interval->m == 0 && $interval->d == 0 && $interval->h == 0 && $interval->i <= 5) {
                        return view('chat.notready', ['booking' => $booking]);
                    }
                } else {
                    $interval = $now->diff($booking_start);
                    if($interval->invert = 1 && $interval->y == 0 && $interval->m == 0 && $interval->d == 0 && $interval->h == 0 && $interval->i >= env('CHAT_FULL_TIME')) {
                        return view('chat.notready', ['booking' => $booking]);
                    }
                }
            } else {
                return view('chat.notfound');
            }
        }

        // Video chat
        $identity = rand(3,1000);
        $client = new Client($this->sid, $this->token);

        $exists = $client->video->rooms->read([ 'uniqueName' => $room->id]);

        if (empty($exists)) {
            $client->video->rooms->create([
                'uniqueName' => $room->id,
                'type' => 'group',
                'recordParticipantsOnConnect' => false
            ]);

            \Log::debug("created new room: ".$room->id);
        }

        $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);

        $videoGrant = new VideoGrant();
        $videoGrant->setRoom($room->id);

        $token->addGrant($videoGrant);
        // End video chat

//		$rooms = $user->rooms;

        // Allow only users that are in the room
//        if(!$room->users->contains('id', $user->id)) return back();

        // Nov 5, 2018 15:37:25
        $start = \Carbon\Carbon::parse($booking->booking_date.' '.$booking->start)->addMinute(-5)->format('M j, Y H:i:s');
        $end = \Carbon\Carbon::parse($booking->booking_date.' '.$booking->end)->addMinute(5)->format('M j, Y H:i:s');

        return view('chat.show', [
            'roomId' => $room->id,
            'room'   => $room,
//            'rooms'  => $rooms,
            'accessToken' => $token->toJWT(),
            'roomName' => $room->id,
            'start' => $start,
            'end' => $end,
        ]);
    }
}
