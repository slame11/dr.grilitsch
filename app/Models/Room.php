<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 29.11.2018
 * Time: 13:39
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    /**
     * Fields that are mass assignable
     *
     * @var array
     */
    protected $fillable = ['booking_id',];

    protected $with = ['users','files'];

    /**
     * A room belongs to many users
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User');
    }

    /**
     * A room has many messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    /**
     * A room has many attached files
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function  files()
    {
        return $this->hasMany('App\Models\ChatFile');
    }
}
