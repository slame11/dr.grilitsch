<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 10.12.2018
 * Time: 13:46
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class ChatFile extends Model
{
    protected $fillable = [
        'room_id','title','description','path',
    ];
}
