<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 27.11.2018
 * Time: 12:25
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'reservation_id',
        'creationDateTime',
        'booking_date',
        'start',
        'end',
        'userTimezone',
        'status',
        'hash',
    ];

    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
