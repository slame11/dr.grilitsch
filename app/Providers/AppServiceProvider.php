<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../../config/admin-lte.php' => config_path('admin-lte.php'),
        ], 'config');
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'admin-lte');
        $this->publishes([
            __DIR__ . '/../../resources/views' => resource_path('views/vendor/admin-lte')
        ], 'views');
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../../config/admin-lte.php', 'admin-lte'
        );
    }
}
