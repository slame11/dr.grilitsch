const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');
mix.js('resources/js/admin-lte.js', 'public/js')
    .sass('resources/sass/admin-lte.scss', 'public/css');
mix.js('resources/js/chat.js', 'public/js');
mix.js('resources/js/dropzone.js', 'public/js')
    .sass('resources/sass/dropzone.scss', 'public/css');

mix.copy('resources/fonts/icomoon', 'public/fonts');
