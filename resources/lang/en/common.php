<?php
/**
 * Created by PhpStorm.
 * User: Aks13
 * Date: 27.11.2018
 * Time: 14:35
 */

return [
    'new' => 'New',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'deleting' => 'Are you sure to delete this record?',
    'empty' => 'Empty data',
    'booking' => 'Booking',
    'bookings' => 'Bookings',
    'id' => 'Id',
    'user' => 'User',
    'users' => 'Users',
    'date' => 'Date',
    'start' => 'Start time',
    'end' => 'End time',
    'status' => 'Status',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'video' => 'Video Call',
    'dashboard' => 'Dashboard',
    'private' => 'Private',
    'hash' => 'Hash',
    'email' => 'Email',
    'created' => 'Created',
    'userTimeZone' => 'User Time Zone',
    'reservations' => 'Reservations'
];
