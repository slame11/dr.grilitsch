<div class="panel panel-default room-list cooldown-custom-height">
    <div class="panel-heading custom-background">
        <span class="flex-center box-header-title"><i class="icon-speedometer-tool mr-5"></i>Chat status:</span>
    </div>

    <div class="panel-body">
        <div id="timer" class="timer">
            <!--  Timer Component  -->
            <Timer
                starttime="{{ $start }}"
                endtime="{{ $end }}"
                trans='{
                 "day":"Day",
                 "hours":"Hours",
                 "minutes":"Minuts",
                 "seconds":"Seconds",
                 "expired":"Event has been expired.",
                 "running":"Till the end of event.",
                 "upcoming":"Till start of event.",
                 "status": {
                    "expired":"Expired",
                    "running":"Running",
                    "upcoming":"Future"
                   }}'
            ></Timer>
            <!--  End! Timer Component  -->
        </div>
    </div>
</div>
