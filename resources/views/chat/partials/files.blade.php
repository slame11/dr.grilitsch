<div class="panel panel-default room-list files-custom-height">
    <div class="panel-heading">
        <span class="flex-center box-header-title"><i class="icon-copy-content mr-5"></i>Attachments:</span>
    </div>

    <div class="panel-body">
        <ul class="files">
            @forelse($room->files as $file)
                <li class="file-item">
                    <span class="name-of-file">
                         <a href="{{ Storage::url($file->path) }}">{{ $file->title }}</a>
                    </span>
                    <span class="delete-file">
                        <a class="btn btn-danger-outline little-icon btn-sm file-delete" href="javascript:" data-file-id="{{ $file->id }}" data-file="{{ $file->path }}">
                            <i class="fa fa-remove"></i>
                        </a>
                    </span>
                </li>
            @empty
                <li><p>{{__('common.empty')}}</p></li>
            @endforelse
        </ul>
    </div>
</div>
