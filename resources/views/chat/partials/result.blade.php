@forelse($room->files as $file)
    <li class="file-item">
                    <span class="name-of-file">
                         <a href="{{ Storage::url($file->path) }}">{{ $file->title }}</a>
                    </span>
        <span class="delete-file">
                        <a class="btn btn-danger-outline little-icon btn-sm file-delete" href="javascript:" data-file-id="{{ $file->id }}" data-file="{{ $file->path }}">
                            <i class="fa fa-remove"></i>
                        </a>
                    </span>
    </li>
@empty
    <li><p>{{__('common.empty')}}</p></li>
@endforelse