<div class="panel panel-default room-list room-list-custom-height">
    <div class="panel-heading custom-background">
        <span class="flex-center box-header-title"><i class="icon-multiple-users-silhouette mr-5"></i>Users:</span>
    </div>

    <div class="panel-body">
        <ul class="private-room-list">
            @forelse($room->users as $user)
            <li>
                <div class="d-flex">
                    <onlineuser v-bind:user="{{ $user }}" v-bind:onlineusers="onlineUsers"></onlineuser>
                    {{ $user->name }}
                </div>
            </li>
            @empty
                <li><p>{{__('common.empty')}}</p></li>
            @endforelse
        </ul>
    </div>
</div>

