@extends('admin.layouts.admin-lte')

@section('content-title', 'Chat')
@section('content-subtitle', '')

@section('breadcrumbs')

    <ol class="breadcrumb">
        <li><a href="{{ route('admin-lte') }}"><i class="fa fa-dashboard"></i> {{ __('common.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-comment-o"></i>
                {{ __('common.private') }}
            chat
        </li>
    </ol>

@endsection

@section('content')

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3">
            <div class="box box-solid no-padding">
                <div class="box-header d-flex justify-between radius-top custom-background">
                    <div class="box-header-wrapper d-flex justify-between background-grey w-100-p">
                        <p class="box-header-title">Members:</p>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <div class="row no-margin">
                        <div class="col-xs-12 no-padding">
                            <div id="media-div">
                                <div id="local-media"></div>
                                <div id="remote-media-div"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-9">
            <div class="box box-solid no-padding"  id="chat">
                <div class="box-body no-padding">
                    <div class="row no-margin is-flex flex-reverse">
                        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 no-padding">
                            <div class="trial-form-layout-left">
                                <input type="hidden" class="room-id" name="room-id" value="{{ $room->id }}">
                                @include('chat.partials.cooldown')
                                @include('chat.partials.room-list')
                                @include('chat.partials.files')
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 no-padding">
                            <div class="trial-form-layout-right">
                                <div class="col-12">
                                    <div class="message-list simple-scrollbar-init">
                                        <chat-messages :messages="messages"
                                                       :user="{{ Auth::user() }}">
                                        </chat-messages>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <chat-form
                                            v-on:messagesent="addMessage"
                                            :user="{{ Auth::user() }}"
                                            :room_id="{{ $roomId }}"
                                    ></chat-form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box box-solid no-padding">
                <div class="box-body no-padding">
                    <div class="row no-margin">
                        <div class="col-xs-12 no-padding">
                            <form action="{{ route('dropzone') }}" class="dropzone" method="post" enctype="multipart/form-data">
                                <input type="hidden" name="room_id" value="{{ $room->id }}">
                                @csrf
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>
    {{--<script src="//media.twiliocdn.com/sdk/js/video/releases/2.0.0-beta5/twilio-video.min.js"></script>--}}
    <script>
        const roomId = {!! json_encode($roomId) !!};

        // Request audio and video tracks
        navigator.mediaDevices.enumerateDevices().then(devices => {
            const videoInput = devices.find(device => device.kind === 'videoinput');
            const audioInput = devices.find(device => device.kind === 'audioinput');
            console.log('System video input device: ',videoInput);
            console.log('System audio input device: ',audioInput);
            if ((videoInput !== undefined)&&(audioInput !== undefined)) {
                return Twilio.Video.createLocalTracks({audio: { deviceId: audioInput.deviceId }, video: {deviceId: videoInput.deviceId, width: 360}});
            }else if ((videoInput !== undefined)&&(audioInput === undefined)) {
                return Twilio.Video.createLocalTracks({audio: true, video: {deviceId: videoInput.deviceId, width: 360}});
            }else if ((videoInput === undefined)&&(audioInput !== undefined)) {
                return Twilio.Video.createLocalTracks({audio: { deviceId: audioInput.deviceId }, });
            }
        }).then(localTracks => {
            return Twilio.Video.connect('{{ $accessToken }}', {
                name: '{{ $roomName }}',
                tracks: localTracks,
            });
        }).then(room => {
            console.log('Successfully joined a Room: ', room.name);

            room.participants.forEach(participantConnected);

            var previewContainer = document.getElementById(room.localParticipant.sid);
            if (!previewContainer || !previewContainer.querySelector('video')) {
                participantConnected(room.localParticipant);
            }

            room.on('participantConnected', function (participant) {
                console.log("Joining: '" + participant.identity + "'");
                participantConnected(participant);
            });

            room.on('participantDisconnected', function (participant) {
                console.log("Disconnected: '" + participant.identity + "'");
                participantDisconnected(participant);
            });
        });

        function participantConnected(participant) {
            console.log('Participant "%s" connected', participant.identity);

            const div = document.createElement('div');
            div.id = participant.sid;
            div.setAttribute("style", "height: 200px; overflow: hidden; margin-bottom: 10px;");
            div.classList.add("col-xs-6");
            div.classList.add("col-sm-6");
            div.classList.add("col-md-6");
            div.classList.add("col-lg-12");

            participant.tracks.forEach(function(track) {
                trackAdded(div, track)
            });

            participant.on('trackAdded', function(track) {
                trackAdded(div, track)
            });
            participant.on('trackRemoved', trackRemoved);

            document.getElementById('media-div').appendChild(div);
        }

        function participantDisconnected(participant) {
            console.log('Participant "%s" disconnected', participant.identity);

            participant.tracks.forEach(trackRemoved);
            document.getElementById(participant.sid).remove();
        }

        function trackAdded(div, track) {
            div.appendChild(track.attach());
            var video = div.getElementsByTagName("video")[0];
            if (video) {
                video.setAttribute("style", "width: 100%; height: 100%; object-fit: cover;");
            }
        }

        function trackRemoved(track) {
            track.detach().forEach( function(element) { element.remove() });
        }

        {{--Twilio.Video.createLocalVideoTrack().then(track => {--}}
            {{--const localMediaContainer = document.getElementById('local-media');--}}
            {{--localMediaContainer.appendChild(track.attach());--}}
        {{--});--}}

        {{--// Request audio and video tracks--}}
        {{--navigator.mediaDevices.enumerateDevices().then(devices => {--}}
            {{--const videoInput = devices.find(device => device.kind === 'videoinput');--}}
            {{--const audioInput = devices.find(device => device.kind === 'audioinput');--}}
            {{--console.log('System video input device: ',videoInput);--}}
            {{--console.log('System audio input device: ',audioInput);--}}
            {{--if ((videoInput !== undefined)&&(audioInput !== undefined)) {--}}
                {{--return Twilio.Video.createLocalTracks({audio: { deviceId: audioInput.deviceId }, video: {deviceId: videoInput.deviceId, width: 360}});--}}
            {{--}else if ((videoInput !== undefined)&&(audioInput === undefined)) {--}}
                {{--return Twilio.Video.createLocalTracks({audio: true, video: {deviceId: videoInput.deviceId, width: 360}});--}}
            {{--}else if ((videoInput === undefined)&&(audioInput !== undefined)) {--}}
                {{--return Twilio.Video.createLocalTracks({audio: { deviceId: audioInput.deviceId }, });--}}
            {{--}--}}
        {{--}).then(localTracks => {--}}
            {{--return Twilio.Video.connect('{{ $accessToken }}', {--}}
                {{--name: '{{ $roomName }}',--}}
                {{--tracks: localTracks,--}}
            {{--});--}}
        {{--}).then(room => {--}}
            {{--console.log('Successfully joined a Room: ', room.name);--}}
            {{--// Log your Client's LocalParticipant in the Room--}}
            {{--const localParticipant = room.localParticipant;--}}
            {{--console.log(`Connected to the Room as LocalParticipant "${localParticipant.identity}"`);--}}

            {{--// Log any Participants already connected to the Room--}}
            {{--room.participants.forEach(participant => {--}}
                {{--console.log(`Participant "${participant.identity}" is connected to the Room`);--}}
            {{--});--}}

            {{--// Log new Participants as they connect to the Room--}}
            {{--room.once('participantConnected', participant => {--}}
                {{--// Attach the Participant's Media to a <div> element.--}}
                {{--room.on('participantConnected', participant => {--}}
                    {{--console.log(`Participant "${participant.identity}" connected`);--}}

                    {{--participant.tracks.forEach(publication => {--}}
                        {{--if (publication.isSubscribed) {--}}
                            {{--const track = publication.track;--}}
                            {{--document.getElementById('remote-media-div').appendChild(track.attach());--}}
                        {{--}--}}
                    {{--});--}}

                    {{--participant.on('trackSubscribed', track => {--}}
                        {{--document.getElementById('remote-media-div').appendChild(track.attach());--}}
                    {{--});--}}
                {{--});--}}
            {{--});--}}

            {{--// Log Participants as they disconnect from the Room--}}
            {{--room.once('participantDisconnected', participant => {--}}
                {{--room.on('disconnected', room => {--}}
                    {{--// Detach the local media elements--}}
                    {{--room.localParticipant.tracks.forEach(publication => {--}}
                        {{--const attachedElements = publication.track.detach();--}}
                        {{--attachedElements.forEach(element => element.remove());--}}
                    {{--});--}}
                {{--});--}}
                {{--console.log(`Participant "${participant.identity}" has disconnected from the Room`);--}}
            {{--});--}}
        {{--});--}}


        setInterval(function() {
            $.post('{{ route('getFiles') }}', {'_token': $('input[name="_token"]').val(), 'room-id': $('.room-id').val()},
                function(result){
                $('.files').html(result);
            });
        }, 5000);

        $(document).on('click','.file-delete',function () {
            $.post('{{ route('deleteFile') }}', {'_token': $('input[name="_token"]').val(), 'room-id': $('.room-id').val(),
                    'file': $(this).attr('data-file'), 'file-id': $(this).attr('data-file-id')},
                function(result){
                    $('.files').html(result);
                });
        });

        {{--@if(Auth::user()->hasRole('User'))--}}
            {{--setInterval(function(){--}}
            {{--if($('.status-tag .text-status').text() === 'Expired') {--}}
                {{--location.reload(true);--}}
            {{--}--}}
        {{--}, 10000);--}}
        {{--@endif--}}

    </script>

    <script src="{{ mix('/js/chat.js') }}"></script>

@stop
