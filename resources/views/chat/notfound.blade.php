@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Verification</div>

                    <div class="card-body">
                        You were not verified!
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
