@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <p>Chat is not available.</p>
                    </div>

                    <div class="card-body">
                        <p>Please check information of your booking!</p>
                        <br />
                        <p>Booking date: {{$booking->booking_date}}</p>
                        <p>Start time: {{$booking->start}} -5 min</p>
                        <p>End time: {{$booking->end}} +5 min</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
