@extends('admin.layouts.admin-lte')

@section('title', __('common.bookings'))
@section('content-title', __('common.bookings'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-lte') }}"><i class="fa fa-dashboard"></i> {{ __('common.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-bookmark"></i> {{ __('common.bookings') }}</li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <table class="table table-condensed table-responsive table-hover">
                <thead>
                <tr>
                    <th>{{ __('common.id') }}</th>
                    <th>{{ __('common.user') }}</th>
                    <th>{{ __('common.email') }}</th>
                    <th>{{ __('common.created') }}</th>
                    <th>{{ __('common.date') }}</th>
                    <th>{{ __('common.start') }}</th>
                    <th>{{ __('common.end') }}</th>
                    <th>{{ __('common.hash') }}</th>
                    <th>{{ __('common.status') }}</th>
                </tr>
                </thead>
                <tbody>
                @forelse($bookings as $booking)
                    <tr>
                        <td>{{ $booking->id }}</td>
                        <td>@if(isset($booking->user->name)){{ $booking->user->name }}@endif</td>
                        <td>@if(isset($booking->user->email)) {{ $booking->user->email }} @endif</td>
                        <td>{{ $booking->creationDateTime }}</td>
                        <td>{{ $booking->booking_date }}</td>
                        <td>{{ $booking->start }}</td>
                        <td>{{ $booking->end }}</td>
                        <td>{{ $booking->hash }}</td>
                        <td>
                            <div class="actions-wrapper">
                                <form action="{{ route('check-room', ['booking' => $booking]) }}" method="post">
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-comments"></i></button>
                                </form>

                                <form onsubmit="if(confirm('Delete?')){ return true }else{ return false }" action="{{ route('bookings.destroy', $booking) }}" method="post">
                                    <input type="hidden" name="_method" value="delete" />
                                    {{csrf_field()}}
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                </form>
                            </div>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td><h2>{{ __('common.empty') }}</h2></td>
                    </tr>
                @endforelse
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        <nav aria-label="page navigation">
                            <ul class="pagination">
                                {{$bookings->links()}}
                            </ul>
                        </nav>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
