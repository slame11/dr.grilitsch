@extends('admin.layouts.admin-lte')

@section('title', __('mail.mails'))
@section('content-title', __('mail.mails'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-lte') }}"><i class="fa fa-dashboard"></i> {{ __('dashboard.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-building"></i> {{ __('mail.mails') }}</li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">

            <table>
                <thead>
                <tr>
                    <th>UID</th>
                    <th>Subject</th>
                    <th>From</th>
                    <th>Attachments</th>
                </tr>
                </thead>
                <tbody>
                @if($paginator->count() > 0)
                    @foreach($paginator as $oMessage)
                        <tr>
                            <td>{{$oMessage->getUid()}}</td>
                            <td>{{$oMessage->getSubject()}}</td>
                            <td>{{$oMessage->getFrom()[0]->mail}}</td>
                            <td>{{$oMessage->getAttachments()->count() > 0 ? 'yes' : 'no'}}</td>
                        </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4">No messages found</td>
                    </tr>
                @endif
                </tbody>
            </table>

            {{$paginator->links()}}
        </div>
    </div>
@endsection
