<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

    <!-- Sidebar Menu -->
        @php
            list($currentRouteType) = explode('.', Route::currentRouteName())
        @endphp
        @section('sidebar-menu')
            <ul class="sidebar-menu" data-widget="tree">
                @can('admin')
                    <li @if (in_array($currentRouteType, ['bookings']) ) class="active" @endif>
                        <a href="{{ route('bookings.index') }}">
                            <i class="fa fa-bookmark"></i>
                            <span>{{ __('common.bookings') }}</span>
                        </a>
                    </li>


                    <li @if (in_array($currentRouteType, ['reservations']) ) class="active" @endif>
                        <a href="{{ route('reservations') }}">
                            <i class="fa fa-envelope"></i>
                            <span>{{ __('common.reservations') }}</span>
                        </a>
                    </li>

                    {{--<li @if (in_array($currentRouteType, ['mails']) ) class="active" @endif>--}}
                        {{--<a href="{{ route('mails') }}">--}}
                            {{--<i class="fa fa-envelope"></i>--}}
                            {{--<span>{{ __('mail.mails') }}</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}

                    <li @if (in_array($currentRouteType, ['users']) ) class="active" @endif>
                        <a href="{{ route('users.index') }}">
                            <i class="fa fa-user-secret"></i>
                            <span>{{ __('common.users') }}</span>
                        </a>
                    </li>
                @endcan
            </ul>
    @show
    <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
