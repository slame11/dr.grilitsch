@extends('admin.layouts.admin-lte')

@section('title', __('common.users'))
@section('content-title', __('common.users'))

@section('breadcrumbs')
    <ol class="breadcrumb">
        <li><a href="{{ route('admin-lte') }}"><i class="fa fa-dashboard"></i> {{ __('common.dashboard') }}</a></li>
        <li class="active"><i class="fa fa-user-secret"></i> {{ __('common.users') }}</li>
    </ol>
@endsection

@section('content')
    <div class="box">
        <div class="box-body">
            <table class="table table-condensed table-responsive table-hover">
                <thead>
                <tr>
                    <th>{{ __('common.user') }}</th>
                    <th>{{ __('common.email') }}</th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            {{--<a href="{{ route('bookings.edit', $booking) }}" class="btn btn-info pull-left btn-sm button_right_shift">--}}
                                {{--<i class="fa fa-edit"></i>--}}
                            {{--</a>--}}

                            <form onsubmit="if(confirm('{{ __('common.deleting') }}')){ return true }else{ return false }" action="{{ route('users.destroy', $user) }}" method="post">
                                <input type="hidden" name="_method" value="delete" />
                                {{csrf_field()}}
                                <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td><h2>{{ __('common.empty') }}</h2></td>
                    </tr>
                @endforelse
                </tbody>
                <tfoot>
                <tr>
                    <td>
                        <nav aria-label="page navigation">
                            <ul class="pagination">
                                {{$users->links()}}
                            </ul>
                        </nav>
                    </td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection
