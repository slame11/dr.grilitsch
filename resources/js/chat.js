require('./bootstrap');
require('jquery-mousewheel');
require('malihu-custom-scrollbar-plugin');

window.Vue = require('vue');

Vue.component('chat-messages', require('./components/ChatMessages.vue'));
Vue.component('chat-form', require('./components/ChatForm.vue'));
Vue.component('onlineuser', require('./components/OnlineUser.vue'));
Vue.component('timer', require('./components/Cooldown.vue'));

const chat = new Vue(
    {
        el: '#chat',

        data: {
            messages: [],
            onlineUsers: '',
        },

        created() {
            console.log('in caht js');
            const userId = $('meta[name="userId"]').attr('content');

            if (userId != null) {
                Echo.join('Online')
                    .here((users) => {
                        console.log('this.onlineUsers',users);
                        this.onlineUsers = users;
                    })
                    .joining((user) => {
                        console.log('Join',user);
                        this.onlineUsers.push(user);
                    })
                    .leaving((user) => {
                        console.log('leaving',user);
                        this.onlineUsers = this.onlineUsers.filter((u) => {u != user});
                    });
            }

            this.fetchMessages();

            Echo.private('chat-' + roomId)
                .listen('MessageSent', (e) => {
                    console.log(e);
                    this.messages.push(
                        {
                            message: e.message.message,
                            user: e.user,
                            roomId: e.roomId,
                            created_at: this.formatDate(e.message.created_at),
                        }
                    );
                });
        },

        methods: {
            fetchMessages() {
                axios.get('/messages/' + roomId).then(response => {
                    const formattedData = response.data.map(message => {
                        message.created_at = this.formatDate(message.created_at);

                        return message;
                    });

                    this.messages = formattedData;
                });
            },

            addMessage(message) {
                const index = this.messages.push(message) - 1;

                axios.post('/messages', message).then(response => {
                    this.messages[index].created_at = this.formatDate(response.data.created_at.date);
                });
            },

            formatDate(date) {
                const now = new Date(date);
                const options = {
                    day:    '2-digit',
                    hour:   '2-digit',
                    minute: '2-digit',
                    month:  'short',
                    second: '2-digit',
                };

                return now.toLocaleDateString("en-GB", options);
            },
        }
    }
);
