try {
    window.$ = window.jQuery = require('jquery');
    require('admin-lte');
    require('admin-lte/bower_components/jquery-knob');
    require('bootstrap-sass');
} catch (e) {}

$(function () {
    $('.simple-scrollbar-init').mCustomScrollbar({
        theme:"dark-2",
        scrollInertia: 300,
    });
});