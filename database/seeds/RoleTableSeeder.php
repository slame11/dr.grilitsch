<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name'        => 'Admin',
                'permissions' => [
                    'admin',
                    'permissions',
                    'roles',
                    'users',
                    'mail',
                    'chat',
                ],
            ],
            [
                'name'        => 'User',
                'permissions' => [
                    'chat',
                ],
            ],
        ];

        foreach ($roles as $data) {
            $role        = new \App\Models\Role();
            $role->name  = $data['name'];
            $role->save();

            foreach ($data['permissions'] as $p) {
                $permission = \Spatie\Permission\Models\Permission::firstOrCreate(
                    ['name' => $p]
                );
                $role->givePermissionTo($permission);
            }
        }
    }
}
