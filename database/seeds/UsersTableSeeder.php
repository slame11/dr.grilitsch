<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Kryukov Andrew',
                'email' => 'aks13@ukr.net',
                'password' => '111111',
                'roles'      => [
                    'Admin',
                ],
            ],

            [
                'name' => 'Kudrashov Oleg',
                'email' => 'slame1121@gmail.com',
                'password' => '111111',
                'roles'      => [
                    'Admin',
                ],
            ],
        ];

        foreach ($users as $data) {
            $user = \App\Models\User::create(
                [
                    'email'      => $data['email'],
                    'name'       => $data['name'],
                    'password'   => $data['password'],
                ]
            );
            foreach ($data['roles'] as $r) {
                $role = \Spatie\Permission\Models\Role::firstOrCreate(['name' => $r]);
                $user->assignRole($role);
            }
        }
    }
}
