<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/verification', 'ChatController@verification');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Chat
Route::post('/messages/checkchat', 'ChatController@checkchat')->name('checkchat');
Route::get('/messages/chat-{room}', 'ChatController@show')->name('chat');
Route::get('/messages/{room}', 'ChatController@fetchMessages');
Route::post('/messages', 'ChatController@sendMessage');
Route::post('/messages/checkroom', 'ChatController@checkRoom')->name('check-room');
Route::post('/messages/dropzone', 'ChatController@dropzone')->name('dropzone');
Route::post('/messages/getFiles', 'ChatController@getFiles')->name('getFiles');
Route::post('/messages/deleteFile', 'ChatController@deleteFile')->name('deleteFile');

Route::get('/srvtime', 'ChatController@srvtime');
//Route::get('/room/join/{roomName}', 'VideoRoomsController@joinRoom');
//Route::post('/room/create', 'VideoRoomsController@createRoom');

Route::get('/admin-lte', 'Admin\AdminController@index')->name('admin-lte');
Route::get('/mails', 'Admin\AdminController@getMails')->name('mails');
Route::get('/reservations', 'Admin\AdminController@getReservations')->name('reservations');
Route::resource('/bookings', 'Admin\BookingController');
Route::resource('/users', 'Admin\UserController');
